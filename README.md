# README #

This repository contains the electronics design and firmware of the robots used by RoboFEI for the RoboCup Small Size League.

### The repository contains ###

 * RoboFEI Futbots2010 main rev.C schematics and lay-out
 * RoboFEI Futbots2010 kick rev.B schematics and lay-out
 * RoboFEI Futbots2010 rev.C Firmware and VHDL modules 
 
 Copyright Robotics and Artificial Intelligence Lab.
[Centro Universitario da FEI](http://www.fei.edu.br/robo)

### License ###
This work is Open Source, distributed under the terms of the CREATIVE COMMONS Attribution NonCommercial (CC-BY-NC) License.

### Who do I talk to? ###

* For questions, please email robofei [at] groups[dot]google[dot].com