------------------------------------------------------------------------------
-- ad7928_logic.vhd - entity/architecture pair
------------------------------------------------------------------------------
--

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

-- DO NOT EDIT ABOVE THIS LINE --------------------

--USER libraries added here

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_REG                    -- Number of software accessible registers
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Addr                  -- Bus to IP address bus
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   Bus2IP_RdCE                  -- Bus to IP read chip enable
--   Bus2IP_WrCE                  -- Bus to IP write chip enable
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity ad7928_logic is
  generic
  (
	 -- CONTROL BITs: WRITE|SEQ|DONTCARE|ADD2|ADD1|ADD0|PM1|PM0|SHADOW|DONTCARE|RANGE|CODING
	 CONTROL: std_logic_vector := "1000001100010000";

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_DWIDTH                   : integer              := 32;
    C_NUM_REG                      : integer              := 6
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
	 Clk 			: in  std_logic;
	 
	 SCLK			: out std_logic;
	 CS				: out std_logic;
	 DIN			: out std_logic;
	 DOUT			: in 	std_logic;
    
	 ALARM			: out std_logic_vector (0 to 7);
	 ALM0 			: out std_logic;
	 ALM1	 		: out std_logic;
	 ALM2			: out std_logic;

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Addr                    : in  std_logic_vector(0 to 31);
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute SIGIS : string;
  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

  TYPE array200int is array (0 to 49) of integer;
  
  
end entity ad7928_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of ad7928_logic is

  	signal Scaled_clk		: std_logic :='0';	
	signal measure			: std_logic_vector(0 to 15);
	signal CONTROLREG 	: std_logic_vector (0 to 15);
	signal tempalarm 		: std_logic_vector (0 to 7) :=(others => '0');
	--signal ALMThreshold 	: std_logic_vector(0 to 11) 	:="100000000000"; --2048, meio da escala
	signal nextChannel 	: std_logic_vector(0 to 2) :=(others => '0');
	
  ------------------------------------------
  -- Signals for user logic slave model s/w accessible register example
  ------------------------------------------
  signal slv_reg0                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg1                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg2                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg3                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg4                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg5                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg_write_sel              : std_logic_vector(0 to 5);
  signal slv_reg_read_sel               : std_logic_vector(0 to 5);
  signal slv_ip2bus_data                : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_read_ack                   : std_logic;
  signal slv_write_ack                  : std_logic;
  
begin

process(tempalarm)
begin
	ALARM <=tempalarm;
	ALM0 <= tempalarm(0);
	ALM1 <= tempalarm(1);
	ALM2 <= tempalarm(2);
end process;

-- Assuming clock = 50MHz
-- divide by 4 makes 12.5Mhz,
-- resulting in about 700/n channels KHz of AD sampling rate (AD uses 16 clock pulses + 2 wait in betweeen)
-- for 6 channels, it's 116 KHz sampling rate
Clock_Divider: process( Clk)
		constant Scale10usek	:integer :=   4;
		variable Scalex		:integer;
		variable Count			:integer := 0;
	begin	   
		Scalex := Scale10usek;
	   
		if rising_edge(Clk) then
			if Count>=Scalex then
			   Scaled_clk <= NOT Scaled_clk;
				Count      := 0;
			else
				Count      := Count+1;			
			end if;
		end if;
   end process Clock_Divider ;


ADRead: process(Scaled_clk)
	variable index 			: integer :=0;
	variable integr_index 	: integer :=0;
	variable nextcycle 		: std_logic :='0'; 
	--variable rcvd_channel : integer range 0 to 7;
	variable rcvd_channel 	: std_logic_vector (0 to 2) :="000";
	variable dummyreads 	: integer range 0 to 2 :=0;
	variable ampere_level 	: array200int;
	variable ALMThreshold 	: integer := 22383;
	variable totalsum 		: integer:=0;
	variable timeoff 		: integer:=0;
begin
	if rising_edge(Scaled_Clk) then
		
		if nextcycle = '0' then
			nextcycle := '1' ;
			
			if index =0 then
				CS <= '0';
				if (dummyreads < 2) then -- performs the two dummy reads at power on, as indicated on the AD7928 datasheet
					CONTROLREG<= (others=>'1');
					dummyreads := dummyreads +1;
				else
					--WRITE|SEQ|DONTCARE|ADD2|ADD1|ADD0|PM1|PM0|SHADOW|DONTCARE|RANGE|CODING & 4bits DISCARD
					--CONTROLREG<=CONTROL(0 to 2) & nextChannel & CONTROL(6 to 15);
					CONTROLREG<= "100" & nextChannel & "1100010000"; --linha igual a comentada acima, que nao funcionou por motivo desconhecido
					nextChannel <= nextChannel + 1;
					if nextChannel = "110" then -- if nextChannel > 5
						nextChannel <= (others =>'0');
					end if;
				end if;
			end if;
			
			if index > 0 and index < 17 then
				SCLK <= '1';
				DIN <= CONTROLREG(index -1);
				measure(index -1) <= DOUT;
			end if;
			
		elsif nextcycle = '1' then
			nextcycle := '0';
			
			SCLK <= '0';
			index := index + 1;
			
			if index = 18 then
				CS <= '1';
				rcvd_channel := measure(1 to 3);
				case rcvd_channel is
				when "000" =>
					slv_reg0 <= "0000000000000000" & measure(0 to 15);
					-- if measure (4) ='1' then
						-- ampere_level(integr_index) :=  CONV_INTEGER(measure(5 to 15));
						-- integr_index := integr_index + 1;
						-- if integr_index = 50 then
							-- integr_index := 0;
						-- end if;
					-- end if;				
				when "001" =>
					slv_reg1 <= "0000000000000000" & measure(0 to 15);
				when "010" =>
					slv_reg2 <= "0000000000000000" & measure(0 to 15);
				when "011" =>
					slv_reg3 <= "0000000000000000" & measure(0 to 15);
				when "100" =>
					slv_reg4 <= "0000000000000000" & measure(0 to 15);
				when "101" =>
					slv_reg5 <= "0000000000000000" & measure(0 to 15);
				when others =>
				end case;
				
				-- -- triggers the current_shut_off alarm
				-- totalsum :=0;
				-- for i in 0 to 49 loop
					-- totalsum :=totalsum + ampere_level(i);
				-- end loop;
				
				-- if totalsum > ALMThreshold then
					-- timeoff := 100;
				-- end if;
				
				-- if timeoff > 0 then
					-- tempalarm(rcvd_channel) <= '1';
					-- timeoff := timeoff - 1;
				-- else
					-- tempalarm(rcvd_channel) <= '0';
					-- --limit_timer(rcvd_channel) := limit_timer(rcvd_channel) -1;
				-- end if;
			end if;
			
			if index > 18 then
				index :=0;
			end if;
			
		end if;	
	end if;		
end process ADRead;


  ------------------------------------------
  -- Example code to read/write user logic slave model s/w accessible registers
  -- 
  -- Note:
  -- The example code presented here is to show you one way of reading/writing
  -- software accessible registers implemented in the user logic slave model.
  -- Each bit of the Bus2IP_WrCE/Bus2IP_RdCE signals is configured to correspond
  -- to one software accessible register by the top level template. For example,
  -- if you have four 32 bit software accessible registers in the user logic,
  -- you are basically operating on the following memory mapped registers:
  -- 
  --    Bus2IP_WrCE/Bus2IP_RdCE   Memory Mapped Register
  --                     "1000"   C_BASEADDR + 0x0
  --                     "0100"   C_BASEADDR + 0x4
  --                     "0010"   C_BASEADDR + 0x8
  --                     "0001"   C_BASEADDR + 0xC
  -- 
  ------------------------------------------
  slv_reg_write_sel <= Bus2IP_WrCE(0 to 5);
  slv_reg_read_sel  <= Bus2IP_RdCE(0 to 5);
  slv_write_ack     <= Bus2IP_WrCE(0) or Bus2IP_WrCE(1) or Bus2IP_WrCE(2) or Bus2IP_WrCE(3) or Bus2IP_WrCE(4) or Bus2IP_WrCE(5);
  slv_read_ack      <= Bus2IP_RdCE(0) or Bus2IP_RdCE(1) or Bus2IP_RdCE(2) or Bus2IP_RdCE(3) or Bus2IP_RdCE(4) or Bus2IP_RdCE(5);

  -- implement slave model software accessible register(s)
--  SLAVE_REG_WRITE_PROC : process( Bus2IP_Clk ) is
--  begin
--
--    if Bus2IP_Clk'event and Bus2IP_Clk = '1' then
--      if Bus2IP_Reset = '1' then
--        slv_reg0 <= (others => '0');
--        slv_reg1 <= (others => '0');
--        slv_reg2 <= (others => '0');
--        slv_reg3 <= (others => '0');
--        slv_reg4 <= (others => '0');
--        slv_reg5 <= (others => '0');
--      else
--        case slv_reg_write_sel is
--          when "100000" =>
--            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
--              if ( Bus2IP_BE(byte_index) = '1' ) then
--                --slv_reg0(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
--              end if;
--            end loop;
--          when "010000" =>
--            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
--              if ( Bus2IP_BE(byte_index) = '1' ) then
--                slv_reg1(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
--              end if;
--            end loop;
--			--when block repeats for all other options
--
--          when others => null;
--        end case;
--      end if;
--    end if;
--
--  end process SLAVE_REG_WRITE_PROC;

  -- implement slave model software accessible register(s) read mux
  SLAVE_REG_READ_PROC : process( slv_reg_read_sel ) is
  begin

    case slv_reg_read_sel is
      when "100000" => 
			slv_ip2bus_data <= slv_reg0;
      when "010000" => 
			slv_ip2bus_data <= slv_reg1;
      when "001000" => 
			slv_ip2bus_data <= slv_reg2;
      when "000100" => 
			slv_ip2bus_data <= slv_reg3;
      when "000010" => 
			slv_ip2bus_data <= slv_reg4;
      when "000001" => 
			slv_ip2bus_data <= slv_reg5;
      when others => 
			slv_ip2bus_data <= (others => '0');
    end case;

  end process SLAVE_REG_READ_PROC;

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= slv_ip2bus_data when slv_read_ack = '1' else
                  (others => '0');

  IP2Bus_WrAck <= slv_write_ack;
  IP2Bus_RdAck <= slv_read_ack;
  IP2Bus_Error <= '0';

end IMP;
