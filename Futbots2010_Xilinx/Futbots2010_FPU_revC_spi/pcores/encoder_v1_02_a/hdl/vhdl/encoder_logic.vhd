------------------------------------------------------------------------------
-- user_logic.vhd - entity/architecture pair
------------------------------------------------------------------------------
--
-- ***************************************************************************
-- ** Copyright (c) 1995-2009 Xilinx, Inc.  All rights reserved.            **
-- **                                                                       **
-- ** Xilinx, Inc.                                                          **
-- ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
-- ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
-- ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
-- ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
-- ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
-- ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
-- ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
-- ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
-- ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
-- ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
-- ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
-- ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
-- ** FOR A PARTICULAR PURPOSE.                                             **
-- **                                                                       **
-- ***************************************************************************
--
------------------------------------------------------------------------------
-- Filename:          user_logic.vhd
-- Version:           1.00.a
-- Description:       User logic.
-- Date:              Sun Jun 13 22:56:35 2010 (by Create and Import Peripheral Wizard)
-- VHDL Standard:     VHDL'93
------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
------------------------------------------------------------------------------

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

-- DO NOT EDIT ABOVE THIS LINE --------------------

--USER libraries added here

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_REG                    -- Number of software accessible registers
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   Bus2IP_RdCE                  -- Bus to IP read chip enable
--   Bus2IP_WrCE                  -- Bus to IP write chip enable
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity encoder_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    --USER generics added here
    -- ADD USER GENERICS ABOVE THIS LINE ---------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_DWIDTH                   : integer              := 32;
    C_NUM_REG                      : integer              := 1
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
	 Clk		: in std_logic;
	 Ch_A		: in std_logic;
	 Ch_B		: in std_logic;
	 --LED_A :out std_logic;
	 --LED_B :out std_logic;
	 --testin: in std_logic;
	 --testout: out std_logic;

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1); 
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute SIGIS : string;
  attribute SIGIS of Bus2IP_Clk    : signal is "CLK"; 
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity encoder_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of encoder_logic is

  signal prevCh_A 	: std_logic :='0';
  signal prevCh_B 	: std_logic :='0';
  signal odom 			: std_logic_vector(0 to 15);
  signal clear_odom	: std_logic;
  signal debounce_Ch_A : std_logic_vector (0 to 2);
  signal debounce_Ch_B : std_logic_vector (0 to 2);
  signal Scaled_Clk		:std_logic :='0';
  
  ------------------------------------------
  -- Signals for user logic slave model s/w accessible register example
  ------------------------------------------
  signal slv_reg_count                  : std_logic_vector(0 to C_SLV_DWIDTH-1); 
  signal slv_reg_interval               : std_logic_vector(0 to C_SLV_DWIDTH-1) :="00000000000000000000000000000001"; 
  signal slv_reg_write_sel              : std_logic_vector(0 to 0);
  signal slv_reg_read_sel               : std_logic_vector(0 to 0);
  signal slv_ip2bus_data                : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_read_ack                   : std_logic;
  signal slv_write_ack                  : std_logic;

begin
Clk_scale: process(Clk)
  
  variable pre_scale : integer :=0;
  variable usec_scale : integer :=0;
  
  begin
		if rising_edge(Clk) then
			pre_scale :=pre_scale + 1;
			if pre_scale = 50 then --assumindo clock the 50MHz, resulta em uma checagem a cada 1us;
				Scaled_Clk <='1';
				pre_scale :=0;
			else
				Scaled_Clk <='0';
			end if;
		end if;
		
	end process Clk_scale;
	
ReadOdometry: process (Scaled_Clk)
	
	variable post_scale : integer :=0;
	variable prev_clear_odom : std_logic :='0';

	--variable leda,ledb : std_logic :='0';
	
	begin
		if rising_edge(Scaled_Clk) then
			debounce_Ch_A(2) <= debounce_Ch_A(1);
			debounce_Ch_B(2) <= debounce_Ch_B(1);
			debounce_Ch_A(1) <= debounce_Ch_A(0);
			debounce_Ch_B(1) <= debounce_Ch_B(0);
			debounce_Ch_A(0) <= Ch_A;
			debounce_Ch_B(0) <= Ch_B;
				--if (debounce_Ch_A ="000" or debounce_Ch_A ="111" or debounce_Ch_B ="000" or debounce_Ch_B ="111") then
			if ((debounce_Ch_A ="000" and (debounce_Ch_B ="000" or debounce_Ch_B ="111")) or
				(debounce_Ch_A ="111" and (debounce_Ch_B ="000" or debounce_Ch_B ="111"))) then 
  
				if Ch_A ='1' and prevCh_A ='0' then
					if Ch_B ='1' then
						odom <= odom + 1; 	--CW (sentido horario)
						--leda := not leda;
					else
						odom <= odom - 1;		--CCW (sentido anti-horario)
						--ledb := not ledb;
					end if;
				end if;
				if Ch_A ='0' and prevCh_A ='1' then
					if Ch_B ='1' then
						odom <= odom - 1; 	--CCW
					else
						odom <= odom + 1;		--CW
					end if;
				end if;
				if Ch_B ='1' and prevCh_B ='0' then
					if Ch_A ='1' then
						odom <= odom - 1; 	--CCW
					else
						odom <= odom + 1;		--CW
					end if;
				end if;
				if Ch_B ='0' and prevCh_B ='1' then
					if Ch_A ='1' then
						odom <= odom + 1; 	--CW
					else
						odom <= odom - 1;		--CCW
					end if;
				end if;
				prevCh_A <= Ch_A;
				prevCh_B <= Ch_B;
			end if;
			
			if (post_scale = slv_reg_interval) then
				slv_reg_count <= "0000000000000000" & odom;
				post_scale :=0;
				odom <=(others=>'0');
			else
				post_scale := post_scale +1;
			end if;
		end if;
		--LED_A <= leda;
		--LED_B <= ledb;
			
end process ReadOdometry;


  slv_reg_write_sel <= Bus2IP_WrCE(0 to 0);
  slv_reg_read_sel  <= Bus2IP_RdCE(0 to 0);
  slv_write_ack     <= Bus2IP_WrCE(0);
  slv_read_ack      <= Bus2IP_RdCE(0);

  writedecode: process(BUS2IP_Clk, Bus2IP_Reset, Bus2IP_WrCE, Bus2IP_Data(16 to 31))
	 begin
         if (rising_edge(BUS2IP_Clk)) then
            if ((Bus2IP_Reset = '0') and (Bus2IP_WrCE(0) = '1') ) then
					slv_reg_interval <= "0000000000000000" & Bus2IP_Data(16 to 31);
            end if;
        end if;
		
end process writedecode;

  -- implement slave model software accessible register(s) read mux
  SLAVE_REG_READ_PROC : process( slv_reg_read_sel, slv_reg_count ) is
  begin

    case slv_reg_read_sel is
      when "1" => 
			slv_ip2bus_data <= slv_reg_count; -- copia o registrador count para o bus de dados
      when others => 
		slv_ip2bus_data <= (others => '0');
    end case;

  end process SLAVE_REG_READ_PROC;

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= slv_ip2bus_data when slv_read_ack = '1' else
                  (others => '0');

  IP2Bus_WrAck <= slv_write_ack;
  IP2Bus_RdAck <= slv_read_ack;
  IP2Bus_Error <= '0';

end IMP;
