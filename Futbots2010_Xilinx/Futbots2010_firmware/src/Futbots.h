#ifndef FUTBOTS_H_
#define FUTBOTS_H_

typedef union {
	float f;
	unsigned char c[sizeof(float)];
}float_char;

typedef union {
	int i;
	unsigned char c[sizeof(int)];
}int_char;

#ifndef PI
#define PI 3.14159265f
#endif

#define REL_CM_PULSO (float)(4320./18.22)	//relacao conversao de cm para pulsos.
											//3.0(rel engrenagens) *1440(pulsos)  = 5.8 * pi(cm)
#define PULSOS_RAD_ROT 1991.2f        		//numero de pulsos da roda por rad de rotacao do robo.
											// (84mm /58mm) * 3.0 * 1440 /pi  ==> (raio centro do robo-eixo da roda/diam da roda) * relacao reducao * odom eixo / pi


// Bloco de defines do chute / chip kick /roller 
#define   DEBUG_TX 0X80
#define   ACK_OK 0X40

#define   HIGH_ROLLER 0X30
#define   MEDIUM_ROLLER 0X20
#define   LOW_ROLLER 0X10


//tempo minimo entre chutes
#define KICK_HOLD_RELOAD_TIMER 2000
// Defines que controlam numero de ciclos os motores se mantem sem novo comando do radio
#define MOTOR_RELOAD_TIMER 400
#define ROLLER_RELOAD_TIMER 400
#define KICK_RELOAD_TIMER 300

//Variaveis Globais
char robot_num;
char robot_rf_ch;
unsigned char yled,rled, gled; //status dos leds 

#endif /*FUTBOTS_H_*/
