
/*
 * Butterworth Lowpass filter
 * 2nd order
 * Direct-Form II, Second-Order Sections
 * Based on MatLab Digital Signal Processing Toolbox code
*/

#include "IIRFilter.h"

/*	
	//parametros controlador de corrente....
	const float scaleconst1 = 5.8412582632137548E-003;
	const float coeff_b1_section1 = 2.5000029783382999E-001;
	const float coeff_b2_section1 = 5.0000059566765997E-001;
	const float coeff_b3_section1 = 2.5000029783382999E-001;
	const float coeff_a2_section1 = -1.8890330793945231E+000;
	const float coeff_a3_section1 = 8.9487434461663407E-001;
*/

	float scaleconst1;
	float coeff_b1_section1;
	float coeff_b2_section1;
	float coeff_b3_section1;
	float coeff_a2_section1;
	float coeff_a3_section1;
	
	
//second-order Butterworth filter
void Butterworth_LP2_Init(bworth_filter *filterptr) {
	
	filterptr->scale1= 0.0;
	filterptr->scaletypeconvert1 = 0.0;
	
	// Section 1 Signals 
	filterptr->a1sum1 = 0.0;
	filterptr->a2sum1 = 0.0;
	filterptr->b1sum1 = 0.0;
	filterptr->b2sum1 = 0.0;
	filterptr->delay_section1[0] =0.0;
	filterptr->delay_section1[1] =0.0;
	filterptr->inputconv1 = 0.0;
	filterptr->a2mul1 = 0.0;
	filterptr->a3mul1 = 0.0;
	filterptr->b1mul1 = 0.0;
	filterptr->b2mul1 = 0.0;
	filterptr->b3mul1 = 0.0;
	filterptr->output_typeconvert = 0.0;
}

float Butterworth_LP2_Process(float filter_in, bworth_filter * filterptr, char type){

	if (type == FILTER_CURRENT){
		scaleconst1 = 9.6543617138663738E-004;
		coeff_b1_section1 = 2.5000000641712167E-001;
		coeff_b2_section1 = 5.0000001283424333E-001;
		coeff_b3_section1 = 2.5000000641712167E-001;
		coeff_a2_section1 = -1.9555782403150355E+000;
		coeff_a3_section1 = 9.5654367651120353E-001;
	}
	else if (type == FILTER_ENCODER){
		scaleconst1 = 1.6474073266704803E-001;
		coeff_b1_section1 = 2.5041491909045011E-001;
		coeff_b2_section1 = 5.0082983818090021E-001;
		coeff_b3_section1 = 2.5041491909045011E-001;
		coeff_a2_section1 = -1.3489677452527946E+000;
		coeff_a3_section1 = 5.1398189421967566E-001;
	}

	filterptr->scale1 = filter_in * scaleconst1;
	filterptr->scaletypeconvert1 = filterptr->scale1;

	//  --   ------------------ Section 1 ------------------

	filterptr->delay_section1[1] = filterptr->delay_section1[0];
	filterptr->delay_section1[0] = filterptr->a1sum1;
	filterptr->inputconv1 = filterptr->scaletypeconvert1;
	filterptr->a2mul1 = filterptr->delay_section1[0] * coeff_a2_section1;
	filterptr->a3mul1 = filterptr->delay_section1[1] * coeff_a3_section1;
	filterptr->b1mul1 = filterptr->a1sum1 * coeff_b1_section1;
	filterptr->b2mul1 = filterptr->delay_section1[0] * coeff_b2_section1;
	filterptr->b3mul1 = filterptr->delay_section1[1] * coeff_b3_section1;
	filterptr->a2sum1 = filterptr->inputconv1 - filterptr->a2mul1;
	filterptr->a1sum1 = filterptr->a2sum1 - filterptr->a3mul1;
	filterptr->b2sum1 = filterptr->b1mul1 + filterptr->b2mul1;
	filterptr->b1sum1 = filterptr->b2sum1 + filterptr->b3mul1;
	filterptr->output_typeconvert = filterptr->b1sum1;

	return filterptr->output_typeconvert;
}
