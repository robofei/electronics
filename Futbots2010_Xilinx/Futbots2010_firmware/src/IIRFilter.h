#ifndef IIRFILTER_H_
#define IIRFILTER_H_

#define FILTER_ENCODER 0x00
#define FILTER_CURRENT 0x01

typedef struct {
	
	float  scale1;
	float  scaletypeconvert1;

	float  a1sum1;
	float  a2sum1;
	float  b1sum1;
	float  b2sum1;
	float  delay_section1[2];
	float  inputconv1;
	float  a2mul1;
	float  a3mul1;
	float  b1mul1;
	float  b2mul1;
	float  b3mul1;
	float  output_typeconvert;
	
	} bworth_filter;
	


/*float  scale1;
float  scaletypeconvert1;

float  a1sum1;
float  a2sum1;
float  b1sum1;
float  b2sum1;
float  delay_section1[2];
float  inputconv1;
float  a2mul1;
float  a3mul1;
float  b1mul1;
float  b2mul1;
float  b3mul1;
float  output_typeconvert;*/

void Butterworth_LP2_Init(bworth_filter * filterptr);
float Butterworth_LP2_Process(float filter_in, bworth_filter * filterptr, char type);

#endif /*IIRFILTER_H_*/
