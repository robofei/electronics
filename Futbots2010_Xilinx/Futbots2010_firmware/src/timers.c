#include "xparameters.h"	//driver parameters
#include "timers.h"

XTmrCtr Timer0; /* Instancia do timer counter */

void Timer_Configure(){
	// canal 0 - uso das fun��es de Delay
	XTmrCtr_Initialize(&Timer0, XPAR_XPS_TIMER_0_DEVICE_ID);
	//u32 options = XTC_DOWN_COUNT_OPTION;
	// canal 1 - uso como cronometro
    XTmrCtr_SetOptions(&Timer0, 1, XTC_AUTO_RELOAD_OPTION);

}

/**
	Delay de milisegundos
*/
void Delay_ms(int x)
  {

	volatile u32 countval = 0xFFFFFFFF - (u32)x * 0x0000C350;
	XTmrCtr_SetResetValue(&Timer0,0,countval);
	XTmrCtr_Start(&Timer0, 0);
	// aguarda o estouro do Timer0
	while(! (XTmrCtr_ReadReg(XPAR_XPS_TIMER_0_BASEADDR,
		       0, XTC_TCSR_OFFSET) & XTC_CSR_INT_OCCURED_MASK));

  }

/**
	Delay de microsegundos
*/
void Delay_us(int x)
  {
	//u32 CounterControlReg;
	volatile u32 countval = 0xFFFFFFFF - (u32)x * 0x00000025;
	XTmrCtr_WriteReg(XPAR_XPS_TIMER_0_BASEADDR, 0, XTC_TLR_OFFSET, countval);
	XTmrCtr_Start(&Timer0, 0);
	// aguarda o estouro do Timer0
	while(! (XTmrCtr_ReadReg(XPAR_XPS_TIMER_0_BASEADDR,
		       0, XTC_TCSR_OFFSET) & XTC_CSR_INT_OCCURED_MASK));
}


/**
 * Inicia um cronometro simples usando o canal 1 do timer
**/
void Cronometer_Start(){
	XTmrCtr_WriteReg(XPAR_XPS_TIMER_0_BASEADDR, 1, XTC_TLR_OFFSET, 0);
	XTmrCtr_Start(&Timer0, 1);
}

void Cronometer_Stop(){
	XTmrCtr_Stop(&Timer0, 1);
}

u32 Cronometer_Read(){
	return XTmrCtr_GetValue(&Timer0, 1);
}

