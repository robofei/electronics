/*
*
* */
#include <stdio.h>
#include "xparameters.h"	//driver parameters
#include "xenv.h"			//Xilinx specific
#include "xstatus.h" 		//Xilinx specific
#include "xbasic_types.h"	//Xilinx specific
#include "xintc.h" 			//Interrupt controller
#include "xgpio.h" 			//General Purpose I/O controller
#include "xspi.h"
#include "xwdttb.h" 		//Watchdog controller
#include "xuartlite.h" 		//Uart lite controller
#include "xuartlite_l.h"	//Uart lite low level functions
#include "xtmrctr.h" 		//Timer controller
//
#include "Futbots.h"
#include "perifericos.h"
#include "timers.h"
#include "math.h"
#include "simple_trig.h"
#include "IIRFilter.h"
#include "stdlib.h"

//Variaveis Globais
#define MAXDEBUGPOINTER 400
unsigned char debugarray[MAXDEBUGPOINTER];
int debugpointer=0;
char capturedebug=0;
int_char fdebug1, fdebug2;
int vref=20.;
unsigned char returnsensor[32];
static int M1=0,M2=0,M3=0,M4=0;
int cont1=0, cont2=0, cont3=0, cont4=0, cont_roller=0;
int cur_roller=0;
float measured_Vx=0,measured_Vy=0,measured_W=0;
bworth_filter motor_ifilter[5]; //current_filters
float measured_dx=0,measured_dy=0,measured_dw=0;
float KP_L=1.0f;

float KP_A=0.2f;
float Kpx=1.0, Kpy=1.0, Kpw=1.0; //constantes para a malha de controle direta

float tempM1,tempM2,tempM3,tempM4; //teste - retirados de process control vec

static unsigned char can_kick;
int kick_delay = 0;
int kick_hold = 0;


XIntc InterruptCtrl0;
//XGpio GpioRadio;
XUartLite UartXbee;
XGpio GpioShootSense;
XGpio GpioNordicradio;
XSpi nordic_radio;
int SPIError=0;

void SpiRadio_IntrHandler(void *CallBackRef, u32 StatusEvent, u32 ByteCount)
{

	/*
	 * Indicate the transfer on the SPI bus is no longer in progress
	 * regardless of the status event.
	 */
	//TransferInProgress = XFALSE;

	/*
	 * If the event was not transfer done, then track it as an error.
	 */
	if (StatusEvent != XST_SPI_TRANSFER_DONE) {
		SPIError++;
	}
	else 
	{
	//	if (rled){
//		LEDr_off;
//		rled=0;
//	}else{
//		LEDr_on;
//		rled=1;
//	}
	}
}

int Startup()
{
	
	u32 Status;
	//LEDs
	GpioOutputExample(XPAR_LEDS_4BIT_DEVICE_ID,3);

	//Inicializa Modulo SPI
    u16 ControlReg;
    Status = XSpi_Initialize(&nordic_radio, XPAR_XPS_SPI_0_RADIO_DEVICE_ID);
    ControlReg = XSpi_GetControlReg(&nordic_radio);
    XSpi_SetControlReg(&nordic_radio, ControlReg | XSP_CR_MASTER_MODE_MASK);
     
    //GPIO Nordic radio
    XGpio_Initialize(&GpioNordicradio, XPAR_XPS_GPIO_0_DEVICE_ID);
    XGpio_SetDataDirection(&GpioNordicradio, 1, 0x0);

	//Shoot and Sensors
	XGpio_Initialize(&GpioShootSense, XPAR_SENSORSANDACTUATORS_4BIT_DEVICE_ID);
	XGpio_SetDataDirection(&GpioShootSense, 1, 0x0C);

	//Configura Timer0
	Timer_Configure();
	//XTmrCtr_Start(&Timer0, 1);
	Cronometer_Start();

	//Configura Controlador de Interrupcoes
	Status = XIntc_Initialize(&InterruptCtrl0, XPAR_INTC_0_DEVICE_ID);

	Status = XIntc_Connect(&InterruptCtrl0, XPAR_XPS_INTC_0_XPS_SPI_0_RADIO_IP2INTC_IRPT_INTR,
		 (XInterruptHandler)XSpi_InterruptHandler, (void*)&nordic_radio);

	//Define a funcao de CallBack para as interrupcoes da SPI
	XSpi_SetStatusHandler(&nordic_radio, &nordic_radio, (XSpi_StatusHandler) SpiRadio_IntrHandler);
	//Habilita a interrupcao da SPI para que as interrupcoes efetivamente ocorram
	XIntc_Enable(&InterruptCtrl0, XPAR_XPS_INTC_0_XPS_SPI_0_RADIO_IP2INTC_IRPT_INTR);

	microblaze_enable_interrupts();
	Status = XIntc_Start(&InterruptCtrl0,XIN_REAL_MODE);

	robot_num = DIP_Switch();
	while (robot_num > 5){
		//sinaliza erro, ja que soh existem 6 robos
		XGpio_WriteReg(XPAR_LEDS_4BIT_BASEADDR, 1,5);
		Delay_ms(100);
		XGpio_WriteReg(XPAR_LEDS_4BIT_BASEADDR, 1,0);
		Delay_ms(100);
		robot_num = DIP_Switch();
	}
	XGpio_WriteReg(XPAR_LEDS_4BIT_BASEADDR, 1,0);

	robot_rf_ch = DIP_Frequency();
	
	MOTOR_EN_ON;

	return 0;
}

float MathMax(float n1, float n2)
{
	if (n1 >=n2)
		return n1;
	else
		return n2;
}

void Inverse_Control(float *delta_Vx, float *delta_Vy, float *delta_W, float m1, float m2, float m3, float m4)
{
  
  *delta_Vx = -0.4590f * m1 + 0.4590f * m2 + 0.4590f * m3 - 0.4590f * m4;
  *delta_Vy = -0.2981f * m1 - 0.2981f * m2 + 0.2981f * m3 + 0.2981f * m4;
  *delta_W = 0.25f * (m1 + m2 + m3 + m4);
}

/*
Malha de controle direta. 
Entradas: velocidades lineares Vx e Vy e a velocidade angular W. 
Saidas:  velocidades de cada um dos motores
*/
void Direct_Control(float *Vx, float *Vy, float *W, int *M1,int *M2,int *M3,int *M4, char new_control_loop)
{
	float max,adjust_factor;
	//float tmp_Vx, tmp_Vy, tmp_W;
	// Taxa de giro do robo - obs: retirada multiplicacao inutil do rcvd_data[2] por PI/180 e depois na funcao 180/PI
	//*W = KP_A * *W; //KP_A = 0.2 ? //retirado 17/03/13 Eduardo
	/*//limitação de W maximo
	if (*W > 10.)
	*W = 10;
	else if (*W < - 10.)
	*W = -10;
	*/
	//tmp_Vx = Kpx * *Vx;
	//tmp_Vy = Kpy * *Vy;
	//tmp_W = Kpw * *W;

	// Calcula as velocidades de cada motor, combinando os modulos de x e y e a velocidade angular
	tempM1 = (float)-0.5446  * *Vx + (float)-0.8387  * *Vy  + *W;
	tempM2 = (float)0.5446  * *Vx + (float)-0.8387  * *Vy  + *W;
	tempM3 = (float)0.5446  * *Vx+ (float)0.8387  * *Vy  + *W;
	tempM4 = (float)-0.5446  * *Vx + (float)0.8387  * *Vy + *W;

	/*Proportional limitation of motor speeds.
	if a given speed exceeds the maximum of motor (PWM=100), all the speeds are scaled down proportionally*/
	max = MathMax (MathMax(fabsf(tempM1) , fabsf(tempM2)) , MathMax(fabsf(tempM3) , fabsf(tempM4)));
	if (max > 100.f)
	adjust_factor = max/100.f;
	else
	adjust_factor = 1.f;

	*M1 = (int)(tempM1 / adjust_factor);
	*M2 = (int)(tempM2 / adjust_factor);
	*M3 = (int)(tempM3 / adjust_factor);
	*M4 = (int)(tempM4 / adjust_factor);
}

void AddtoDebugdata(unsigned char debugvar[]){
	int n=0;
	for (n=sizeof(int); n>0; n--){ //ordem de copia do char eh decrescente pq Microblaze usa big endian, contrario do PC
		debugarray[debugpointer++] = debugvar[n-1];
	}
}
void AddtoReturnSensor(unsigned char debugvar[], int index, int size){
	int n;
	for (n=0; n<size; n++){ //ordem de copia do char eh decrescente pq Microblaze usa big endian, contrario do PC
		returnsensor[index + n] = debugvar[size-1 - n];
	}
}

int main() 
{

	//static XIntc intc;
	//static XWdtTb xps_timebase_wdt_0_Wdttb;
	//static XTmrCtr xps_timer_0_Timer;

	XCACHE_ENABLE_ICACHE();
	XCACHE_ENABLE_DCACHE();

	unsigned char idlecounter=0;
	//unsigned char counter=0;
	unsigned char tx_counter=0;
	unsigned char packetID=0, prev_packetID=0;
	int rcvd_data[14]={0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	unsigned char optbyte = 0, optbyte2=0;
	//unsigned char pkt_type;
	int batt_level=0;
	char low_batt=0;
	char sync_radio=0;
	char kicksensor=0;
	int motor_on=0, roller_on=0, kick_on=0;
	int custom_kick_delay =100;
	can_kick = 1;
    //int goal_r=0, goal_rho=0, goal_theta=0;
    float r=0,rho=0,theta=0;
    float dX=0,dY=0;

    float tmpVx,tmpVy, tmpW;
	float Vx=0, Vy=0, W=0;

    unsigned char beatcounter=0;
    unsigned char roller_loopcounter=0;
	unsigned char new_control_loop =0;

	int dbgsend_position=0;
	//unsigned char radio_counter=0;
	
	float_char rcv_float;
	int roller_spd=0;
	int pkt_sensortype =0;
	
	Startup();

	//int totalrx=0;
	int sumcont1 = 0; int sumcont2 = 0; int sumcont3 = 0; int sumcont4 = 0;
	
	u32 Status;
	Status = XSpi_Start(&nordic_radio);  //Inicia a Operacao da SPI
    Status = XSpi_SetSlaveSelect(&nordic_radio, 0x1); //Seleciona o radio como slave do bus SPI

	//Config Nordic
	char radio_oper_mode = _NRF24L01P_CONFIG_PRIM_RX;
	Nordic_RadioConfig(&nordic_radio, robot_num, robot_rf_ch, radio_oper_mode); 
	
    	
	//Config do intervalo de amostragem da odometria (em us)
	XGpio_WriteReg (XPAR_ENCODER_1_BASEADDR, 0, 1500);
    XGpio_WriteReg (XPAR_ENCODER_2_BASEADDR, 0, 1500);
    XGpio_WriteReg (XPAR_ENCODER_3_BASEADDR, 0, 1500);
	XGpio_WriteReg (XPAR_ENCODER_4_BASEADDR, 0, 1500);
	
	//setando os offsets dos sensores
	Set_curr_offset();

	//Cronometer_Start(); //timer para registros de tempo do robo

	while(1){ //loop principal
		int bytes_rcvd = Nordic_GetData(&nordic_radio, rcvd_data, &packetID); //le dados mais recentes enviados pelo radio
		//if(packetID != prev_packetID){
		if (bytes_rcvd >0){
			prev_packetID = packetID;
			idlecounter = 0;
/*
    	    r =  (float)rcvd_data[0]  * REL_CM_PULSO; //r enviado convertido de cm para pulsos
			//rho
			rcv_float.c[3] = rcvd_data[9];
			rcv_float.c[2] = rcvd_data[10];
			rcv_float.c[1] = rcvd_data[11];
			rcv_float.c[0] = rcvd_data[12];
			rho = rcv_float.f * PI/180.f;
	        theta = (float)rcvd_data[2] * PI/180.f *2.f;  //goal_theta deve ser multiplicado por 2 pois foi dividido antes do envio, no pc
*/

			Vx = (float)rcvd_data[0];
			Vy = (float)rcvd_data[1];
			W = (float)rcvd_data[2] *2.f;//dividido por 2 antes do envio
			vref = (int)rcvd_data[3];

			optbyte = rcvd_data[4];
			optbyte2 = rcvd_data[5];
			sync_radio = rcvd_data[6] & 0x80;
			
	        //custom_kick_delay = rcvd_data[7] * 100;
			custom_kick_delay = rcvd_data[7] * 60;
	        if (custom_kick_delay > MAX_KICKBRD_DELAY) 
		        custom_kick_delay = MAX_KICKBRD_DELAY;

   	        if (rcvd_data[8])
   	        	roller_spd = rcvd_data[8];
   	        else
	        	roller_spd = 20;

	        //KP_A = ((float)rcvd_data[8])/10.;
   	        //float kp = ((float)rcvd_data[10])/10.;
   	        //float kd = ((float)rcvd_data[11])/10.;
   	        //SetPIDPars(kp,kd);
   	        		           	
	        /*dY = r * simple_sin(rho+PI/2.f);
	        dX = r * simple_cos(rho+PI/2.f);
			new_control_loop=1;*/
			
			motor_on = MOTOR_RELOAD_TIMER;
			roller_on = ROLLER_RELOAD_TIMER;
			kick_on = KICK_RELOAD_TIMER;

			if (optbyte & DEBUG_TX){
				capturedebug=1;
				//LEDr_on;
			}else{
				capturedebug=0;
				//LEDr_off;
			}			
		}
		
		Vx = (Vx*vref)/100;// valores setados em relação ao valor de velocidade de referência 
		Vy = (Vy*vref)/100;
		
		/* Não tem realimentação de velocidade da malha inversa */
		tmpVx = Vx;
		tmpVy = Vy;
		tmpW = W;

		float ang_torque_factor;
				
		if (motor_on > 1)
	    {
        	ang_torque_factor = 10.f * fabs(simple_sin(rho));

        	Direct_Control(&tmpVx, &tmpVy, &tmpW, &M1, &M2, &M3, &M4, new_control_loop);

    	    if (new_control_loop){
	          new_control_loop=0;
    	    }
	     }
	     else
    	 {
	        M1=0;
    	    M2=0;
        	M3=0;
	        M4=0;
	     }
		if (motor_on >0)
		{
			//Ciclos PID
			motor_on--;

			cont1 = PIDMotor(1,M1);
			cont2 = PIDMotor(2,M2);
			cont3 = PIDMotor(3,M3);
			cont4 = PIDMotor(4,M4);

		}
		else
		{
			cont1=cont2=cont3=cont4=0;
		}

		// bloco responsavel pelo controle dos chutes
		// pode ser que o pc envie por mais de um frame a informacao de chute. Sendo assim,
		// optbyte e last optbyte criam um acionamento por borda e nao por nivel
		unsigned char kick_status;
		
		if (!BallSensor_Status() && can_kick) {	
			if(optbyte & (HIGH_KICK | MEDIUM_KICK | LOW_KICK)) { //KICK Ativado
				kick_status = optbyte & (HIGH_KICK | MEDIUM_KICK | LOW_KICK);
				if(kick_status == HIGH_KICK)
					kick_delay = HIGH_KICK_DELAY;
				else if(kick_status == MEDIUM_KICK)
					kick_delay = custom_kick_delay;
				else if(kick_status == LOW_KICK)
					kick_delay = LOW_KICK_DELAY;

				KickBrd_Activate(0,kick_delay);
				kick_hold = KICK_HOLD_RELOAD_TIMER;
				can_kick = 0;
				optbyte &= ~(HIGH_KICK | MEDIUM_KICK | LOW_KICK); //remove o flag de kick, ja que foi executado

			}else if((optbyte & (HIGH_CHIP | MEDIUM_CHIP | LOW_CHIP))) { //CHIP KICK Ativado
				kick_status = optbyte & (HIGH_CHIP | MEDIUM_CHIP | LOW_CHIP);
				if(kick_status == HIGH_CHIP)
					kick_delay = HIGH_CHIP_DELAY;
				else if(kick_status == MEDIUM_CHIP)
					kick_delay = custom_kick_delay;
				else if(kick_status == LOW_CHIP)
					kick_delay = LOW_CHIP_DELAY;

				KickBrd_Activate(1,kick_delay);
				kick_hold = KICK_HOLD_RELOAD_TIMER;
				can_kick = 0;				
				optbyte &= ~(HIGH_CHIP | MEDIUM_CHIP | LOW_CHIP); //remove o flag de kick, ja que foi executado

			}
		}
		// CRIA TEMPO MINIMO ENTRE CHUTES para proteger placa de chute
		//na ausencia do sinal de que esta carregado
		if (kick_hold > 0){
			kick_hold--;
		}
		else if( kick_hold == 0)
		{
			can_kick = 1;
		}
		
		// bloco responsavel pelo controle do roller
		if (roller_loopcounter++ == 10){ //para que a malha execute mais lentamente devido a baixa resolucao do contador (sensor hall)
			roller_loopcounter=0;
			if((optbyte & 0x30) && roller_on >0) {
				if ((optbyte & 0x30) == HIGH_ROLLER){
					cont_roller = PIDRoller(roller_spd);
					//SetRoller(roller_spd); //for test
				}
				roller_on -=10; //usar mesmo num que o divisor de beatcounter
			}
			else{
				SetRoller(0);
				roller_on = 0;
			}
		}
		
		if (kick_on > 0)
			kick_on --;
		else
			optbyte &= ~(HIGH_KICK | MEDIUM_KICK | LOW_KICK | HIGH_CHIP | MEDIUM_CHIP | LOW_CHIP);
		
		if (!BallSensor_Status()){
			LEDr_on;
			kicksensor = 1;
		}
		else
		{
			LEDr_off;
			kicksensor = 0;
		}

		//Coleta informacoes para envio ao PC
		//cur_roller = (int)Butterworth_LP2_Process((float)Read_AD7928(5), motor_ifilter[5],FILTER_CURRENT);
		//fdebug2.i = cont_roller;
		returnsensor[0] = kicksensor;
		returnsensor[1] = robot_num;
		returnsensor[2] = (unsigned char)(batt_level/10); //divide por 10 para caber em um char
		returnsensor[3] = pkt_sensortype;
		if (pkt_sensortype == 0){
			fdebug1.i = cont1;
			AddtoReturnSensor(fdebug1.c,4,sizeof(int));
			//fdebug1.i = M1;
			fdebug1.i = cont2;
			AddtoReturnSensor(fdebug1.c,8,sizeof(int));
			//fdebug1.i = return_error(1);
			fdebug1.i = cont3;
			AddtoReturnSensor(fdebug1.c,12,sizeof(int));
			//fdebug1.i = return_error(1)*7;
			fdebug1.i = cont4;
			AddtoReturnSensor(fdebug1.c,16,sizeof(int));
		}
		else
		{
			int n;
			for (n = 0; n < 4; n++){
				fdebug1.i = (int)Read_Current(n);
				AddtoReturnSensor(fdebug1.c,4+4*n,sizeof(int));
			}
		}
		int returnsensor_size = 24;
		//Envio das informacoes de status para o PC
		if (sync_radio){
		//if (0){ //test
			sumcont1 = 0; sumcont2 = 0; sumcont3 = 0; sumcont4 = 0;
			Nordic_Transmit(&nordic_radio, &returnsensor[0]);
			//NO delay - nao adiciona delay porque o radio ja gasta 1.5ms transmitindo
			
			if (pkt_sensortype++ == 1) //alterna entre envio dos diferentes sensores
				pkt_sensortype =0;
		}
		else
		{
			Delay_ms(1);
			Delay_us(500);  //delay proposital para formar um ciclo de +/- 3ms
		}
		//Delay_ms(3);  //delay proposital para formar um ciclo de +/- 3ms
		
		if ((debugpointer < (MAXDEBUGPOINTER - 15)) && (capturedebug==1))
		{
			fdebug1.i = cont1;
			AddtoDebugdata(fdebug1.c);
			fdebug1.i = cont2;
			AddtoDebugdata(fdebug1.c);
			fdebug1.i = cont3;
			AddtoDebugdata(fdebug1.c);    	
			fdebug1.i = cont4;
			AddtoDebugdata(fdebug1.c);
		}

		//Transmissao dos logs ao PC
		if (sync_radio && debugpointer >= MAXDEBUGPOINTER && capturedebug==0){
			LEDr_on;
			if (dbgsend_position < MAXDEBUGPOINTER){
				Nordic_Transmit(&nordic_radio, &debugarray[dbgsend_position]);
				if (optbyte2 != tx_counter){
					dbgsend_position += 16; 
					tx_counter++;
				}
			}
			else
			{
				debugpointer=0;
				dbgsend_position =0;
				tx_counter=0;
				LEDr_off;
			}
		}
		
		//reseta o flag de envio de infos ao PC 
		sync_radio = 0;
		
		//led de atividade, indicacao de bateria e outras checagens de baixa prioridade
		if (beatcounter++ == 250){
			if (robot_num != DIP_Switch()){ //re-checa o numero do robo no DIP_switch
				robot_num = DIP_Switch();
				Nordic_SetRXAddr(&nordic_radio, robot_num);
			}
			if (robot_rf_ch != DIP_Frequency()){ //re-checa o canal RF no DIP_switch
				robot_rf_ch = DIP_Frequency();
				Nordic_SetRF_Channel(&nordic_radio, robot_rf_ch);
			}
				
			batt_level = Read_AD7928(0); //mede a tensao da bateria
			if (batt_level > 800) //800 eh a leitura do AD para 11.0V. o fundo de escala do AD eh 4096 para 5.0V de ref. o divisor resist da leitura de bateria eh 1/11
				low_batt = 0; 	
			else
				low_batt = 1;
				
			if (idlecounter++ == 100){ //quando o robo nao esta recebendo dados, mantem envio periodico do status da bateria
				idlecounter = 0;
				Nordic_Transmit(&nordic_radio, &returnsensor[0]);
			}
			
		}
		if ((low_batt && (beatcounter == 0 || beatcounter == 50 || beatcounter == 100))
				|| (!low_batt && beatcounter ==0)){
			if (yled){
				LEDy_off;
				yled=0;
			}else{
				LEDy_on;
				yled=1;
			}
		}

	}//end loop principal	
}



