#ifndef PERIFERICOS_H_
#define PERIFERICOS_H_

// LEDs y = yellow, r = red, g = green
#define LEDy_on		gpio_bit_write(XPAR_LEDS_4BIT_BASEADDR, 0,1)
#define LEDy_off	gpio_bit_write(XPAR_LEDS_4BIT_BASEADDR, 0,0)
#define LEDr_on		gpio_bit_write(XPAR_LEDS_4BIT_BASEADDR, 1,1)
#define LEDr_off	gpio_bit_write(XPAR_LEDS_4BIT_BASEADDR, 1,0)
#define LEDg_on		gpio_bit_write(XPAR_LEDS_4BIT_BASEADDR, 2,1)
#define LEDg_off	gpio_bit_write(XPAR_LEDS_4BIT_BASEADDR, 2,0)
#define LEDb_on		gpio_bit_write(XPAR_LEDS_4BIT_BASEADDR, 3,1)
#define LEDb_off	gpio_bit_write(XPAR_LEDS_4BIT_BASEADDR, 3,0)

//Motor and Charge Enable
#define MOTOR_EN_ON		gpio_bit_write(XPAR_SENSORSANDACTUATORS_4BIT_BASEADDR, 2,0)
#define MOTOR_EN_OFF	gpio_bit_write(XPAR_SENSORSANDACTUATORS_4BIT_BASEADDR, 2,1)
#define CHARGE_EN_ON	gpio_bit_write(XPAR_SENSORSANDACTUATORS_4BIT_BASEADDR, 3,0)
#define CHARGE_EN_OFF	gpio_bit_write(XPAR_SENSORSANDACTUATORS_4BIT_BASEADDR, 3,1)

//Nordic Radio 
#define R_RX_PAYLOAD 0x61
#define W_TX_PAYLOAD 0xA0
#define R_REGISTER 0x00
#define W_REGISTER 0x20 
 
#define Nordic_CE    0x0
 
#define Nordic_CE_off 	gpio_bit_write(XPAR_XPS_GPIO_0_BASEADDR, Nordic_CE,0)
#define Nordic_CE_on  	gpio_bit_write(XPAR_XPS_GPIO_0_BASEADDR, Nordic_CE,1)

#define NRF24L01P_TX_PWR_ZERO_DB         0
#define NRF24L01P_TX_PWR_MINUS_6_DB     -6
#define NRF24L01P_TX_PWR_MINUS_12_DB   -12
#define NRF24L01P_TX_PWR_MINUS_18_DB   -18

#define NRF24L01P_DATARATE_250_KBPS    250
#define NRF24L01P_DATARATE_1_MBPS     1000
#define NRF24L01P_DATARATE_2_MBPS     2000

#define NRF24L01P_CRC_NONE               0
#define NRF24L01P_CRC_8_BIT              8
#define NRF24L01P_CRC_16_BIT            16

#define NRF24L01P_MIN_RF_FREQUENCY    2400
#define NRF24L01P_MAX_RF_FREQUENCY    2525

#define NRF24L01P_PIPE_P0                0
#define NRF24L01P_PIPE_P1                1
#define NRF24L01P_PIPE_P2                2
#define NRF24L01P_PIPE_P3                3
#define NRF24L01P_PIPE_P4                4
#define NRF24L01P_PIPE_P5                5

/**
* Default setup for the nRF24L01+, based on the Sparkfun "Nordic Serial Interface Board"
*  for evaluation (http://www.sparkfun.com/products/9019)
*/
#define DEFAULT_NRF24L01P_ADDRESS       ((unsigned long long) 0xE7E7E7E7E7 )
#define DEFAULT_NRF24L01P_ADDRESS_WIDTH  5
#define DEFAULT_NRF24L01P_CRC            NRF24L01P_CRC_8_BIT
#define DEFAULT_NRF24L01P_RF_FREQUENCY  (NRF24L01P_MIN_RF_FREQUENCY + 2)
#define DEFAULT_NRF24L01P_DATARATE       NRF24L01P_DATARATE_1_MBPS
#define DEFAULT_NRF24L01P_TX_PWR         NRF24L01P_TX_PWR_ZERO_DB
#define DEFAULT_NRF24L01P_TRANSFER_SIZE  4


#define _NRF24L01P_TX_FIFO_COUNT   3
#define _NRF24L01P_RX_FIFO_COUNT   3

#define _NRF24L01P_TX_FIFO_SIZE   32
#define _NRF24L01P_RX_FIFO_SIZE   32

#define _NRF24L01P_SPI_MAX_DATA_RATE     10000000

#define _NRF24L01P_SPI_CMD_RD_REG            0x00
#define _NRF24L01P_SPI_CMD_WR_REG            0x20
#define _NRF24L01P_SPI_CMD_RD_RX_PAYLOAD     0x61   
#define _NRF24L01P_SPI_CMD_WR_TX_PAYLOAD     0xa0
#define _NRF24L01P_SPI_CMD_FLUSH_TX          0xe1
#define _NRF24L01P_SPI_CMD_FLUSH_RX          0xe2
#define _NRF24L01P_SPI_CMD_REUSE_TX_PL       0xe3
#define _NRF24L01P_SPI_CMD_R_RX_PL_WID       0x60
#define _NRF24L01P_SPI_CMD_W_ACK_PAYLOAD     0xa8
#define _NRF24L01P_SPI_CMD_W_TX_PYLD_NO_ACK  0xb0
#define _NRF24L01P_SPI_CMD_NOP               0xff


#define _NRF24L01P_REG_CONFIG                0x00
#define _NRF24L01P_REG_EN_AA                 0x01
#define _NRF24L01P_REG_EN_RXADDR             0x02
#define _NRF24L01P_REG_SETUP_AW              0x03
#define _NRF24L01P_REG_SETUP_RETR            0x04
#define _NRF24L01P_REG_RF_CH                 0x05
#define _NRF24L01P_REG_RF_SETUP              0x06
#define _NRF24L01P_REG_STATUS                0x07
#define _NRF24L01P_REG_OBSERVE_TX            0x08
#define _NRF24L01P_REG_RPD                   0x09
#define _NRF24L01P_REG_RX_ADDR_P0            0x0a
#define _NRF24L01P_REG_RX_ADDR_P1            0x0b
#define _NRF24L01P_REG_RX_ADDR_P2            0x0c
#define _NRF24L01P_REG_RX_ADDR_P3            0x0d
#define _NRF24L01P_REG_RX_ADDR_P4            0x0e
#define _NRF24L01P_REG_RX_ADDR_P5            0x0f
#define _NRF24L01P_REG_TX_ADDR               0x10
#define _NRF24L01P_REG_RX_PW_P0              0x11
#define _NRF24L01P_REG_RX_PW_P1              0x12
#define _NRF24L01P_REG_RX_PW_P2              0x13
#define _NRF24L01P_REG_RX_PW_P3              0x14
#define _NRF24L01P_REG_RX_PW_P4              0x15
#define _NRF24L01P_REG_RX_PW_P5              0x16
#define _NRF24L01P_REG_FIFO_STATUS           0x17
#define _NRF24L01P_REG_DYNPD                 0x1c
#define _NRF24L01P_REG_FEATURE               0x1d

#define _NRF24L01P_REG_ADDRESS_MASK          0x1f

// CONFIG register:
#define _NRF24L01P_CONFIG_PRIM_RX        (1<<0)
#define _NRF24L01P_CONFIG_PWR_UP         (1<<1)
#define _NRF24L01P_CONFIG_CRC0           (1<<2)
#define _NRF24L01P_CONFIG_EN_CRC         (1<<3)
#define _NRF24L01P_CONFIG_MASK_MAX_RT    (1<<4)
#define _NRF24L01P_CONFIG_MASK_TX_DS     (1<<5)
#define _NRF24L01P_CONFIG_MASK_RX_DR     (1<<6)

#define _NRF24L01P_CONFIG_CRC_MASK       (_NRF24L01P_CONFIG_EN_CRC|_NRF24L01P_CONFIG_CRC0)
#define _NRF24L01P_CONFIG_CRC_NONE       (0)
#define _NRF24L01P_CONFIG_CRC_8BIT       (_NRF24L01P_CONFIG_EN_CRC)
#define _NRF24L01P_CONFIG_CRC_16BIT      (_NRF24L01P_CONFIG_EN_CRC|_NRF24L01P_CONFIG_CRC0)
#define _NRF24L01P_CONFIG_PRIM_TX		 ~_NRF24L01P_CONFIG_PRIM_RX

// EN_AA register:
#define _NRF24L01P_EN_AA_NONE            0

// EN_RXADDR register:
#define _NRF24L01P_EN_RXADDR_NONE        0

// SETUP_AW register:
#define _NRF24L01P_SETUP_AW_AW_MASK      (0x3<<0)
#define _NRF24L01P_SETUP_AW_AW_3BYTE     (0x1<<0)
#define _NRF24L01P_SETUP_AW_AW_4BYTE     (0x2<<0)
#define _NRF24L01P_SETUP_AW_AW_5BYTE     (0x3<<0)

// SETUP_RETR register:
#define _NRF24L01P_SETUP_RETR_NONE       0

// RF_SETUP register:
#define _NRF24L01P_RF_SETUP_RF_PWR_MASK          (0x3<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_0DBM          (0x3<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_MINUS_6DBM    (0x2<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_MINUS_12DBM   (0x1<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_MINUS_18DBM   (0x0<<1)
#define _NRF24L01P_RF_SETUP_RF_DR_HIGH_BIT       (1 << 3)
#define _NRF24L01P_RF_SETUP_RF_DR_LOW_BIT        (1 << 5)
#define _NRF24L01P_RF_SETUP_RF_DR_MASK           (_NRF24L01P_RF_SETUP_RF_DR_LOW_BIT|_NRF24L01P_RF_SETUP_RF_DR_HIGH_BIT)
#define _NRF24L01P_RF_SETUP_RF_DR_250KBPS        (_NRF24L01P_RF_SETUP_RF_DR_LOW_BIT)
#define _NRF24L01P_RF_SETUP_RF_DR_1MBPS          (0)
#define _NRF24L01P_RF_SETUP_RF_DR_2MBPS          (_NRF24L01P_RF_SETUP_RF_DR_HIGH_BIT)

// STATUS register:
#define _NRF24L01P_STATUS_TX_FULL        (1<<0)
#define _NRF24L01P_STATUS_RX_P_NO        (0x7<<1)
#define _NRF24L01P_STATUS_MAX_RT         (1<<4)
#define _NRF24L01P_STATUS_TX_DS          (1<<5)
#define _NRF24L01P_STATUS_RX_DR          (1<<6)

// RX_PW_P0..RX_PW_P5 registers:
#define _NRF24L01P_RX_PW_Px_MASK         0x3F
#define _NRF24L01P_TIMING_Tundef2pd_us     100000   // 100mS
#define _NRF24L01P_TIMING_Tstby2a_us          130   // 130uS
#define _NRF24L01P_TIMING_Thce_us              10   //  10uS
#define _NRF24L01P_TIMING_Tpd2stby_us        4500   // 4.5mS worst case
#define _NRF24L01P_TIMING_Tpece2csn_us          4   //   4uS

//Canais de Operacao do Radio
#define _NRF24L01P_CHANNEL_A_RX	95
#define _NRF24L01P_CHANNEL_A_TX	90

#define _NRF24L01P_CHANNEL_B_RX	75
#define _NRF24L01P_CHANNEL_B_TX	70

#define RADIO_PACKET_SIZE 	32
#define TX_PACKET_SIZE 32
#define PREAMBLE_SIZ 7


// Analog to Digital (ADC) Converter
#define ADC_SCLK	0x1
#define ADC_CS 		0x2
#define ADC_DIN		0x4
#define ADC_DOUT	0x8

#define ADC_SCLK_off 		gpio_bit_write(XPAR_AD_INTERFACE_BASEADDR, 0,0)
#define ADC_SCLK_on 		gpio_bit_write(XPAR_AD_INTERFACE_BASEADDR, 0,1)
#define ADC_CS_off	 		gpio_bit_write(XPAR_AD_INTERFACE_BASEADDR, 1,0)
#define ADC_CS_on 			gpio_bit_write(XPAR_AD_INTERFACE_BASEADDR, 1,1)
#define ADC_DIN_off 		gpio_bit_write(XPAR_AD_INTERFACE_BASEADDR, 2,0)
#define ADC_DIN_on 			gpio_bit_write(XPAR_AD_INTERFACE_BASEADDR, 2,1)
#define ADC_DOUT_off 		gpio_bit_write(XPAR_AD_INTERFACE_BASEADDR, 3,0)
#define ADC_DOUT_on 		gpio_bit_write(XPAR_AD_INTERFACE_BASEADDR, 3,1)

#define KICK_off 			gpio_bit_write(XPAR_SHOOTANDSENSORS_4BIT_BASEADDR, 0,0)
#define KICK_on 			gpio_bit_write(XPAR_SHOOTANDSENSORS_4BIT_BASEADDR, 0,1)
#define CHIP_off 			gpio_bit_write(XPAR_SHOOTANDSENSORS_4BIT_BASEADDR, 1,0)
#define CHIP_on 			gpio_bit_write(XPAR_SHOOTANDSENSORS_4BIT_BASEADDR, 1,1)


#define MOTOR_DEAD_ZONE_BASE 50

//Function Prototypes
//void gLED_Toggle();
//void yLED_Toggle();
void gpio_bit_write (unsigned long baseaddr, unsigned long bit_index, unsigned long new_val);
char DIP_Switch();
char DIP_Frequency();
char BallSensor_Status();
char ShootPower_Status();
void KickBrd_Activate(int mode, int active_timer);
int Read_AD7928(int channel);
void Set_curr_offset(); //add eduardo
float Read_Current (int n); //add eduardo
//
void SetDeadZone(int val);
void SetRoller(int val);
void SetMotor(char nmotor, int val);
void SetPIDPars(float _kp, float _kd);
int PIDMotor(char nmotor, int setpoint);
int PIDRoller(int setpoint);

void Xbee_Transmit(XUartLite * UartInstance, unsigned char * input, unsigned char counter);
int Get_XbeeData(XUartLite * UartInstance, int in[14], unsigned char *packetID);

void Nordic_SetRXAddr(XSpi * SPIRadioPtr, char robot_num);


void KickBrd_Control(unsigned char kick_status);

#define MAX_RX_BUF_SIZE 256 //buffer de RX da UART
struct UartDataStruct {
u8 RecvBuffer[MAX_RX_BUF_SIZE];
u8 TxBuffer[MAX_RX_BUF_SIZE];
unsigned int RecvBytes;
};

//volatile struct UartDataStruct UartData;

void Nordic_RadioConfig(XSpi * SPIRadioPtr, char robot_num, char frequency, char oper_mode);
void Nordic_Transmit(XSpi * SPIRadioInstance, unsigned char * input);
int Nordic_GetData(XSpi * SPIRadioInstance, int info[14], unsigned char *packetID);
inline void Nordic_readreg(XSpi * SPIRadioPtr, unsigned char reg);
inline void Nordic_writereg(XSpi * SPIRadioPtr, unsigned char reg, unsigned char value);
void Nordic_SetTransceiverMode(XSpi* SPIRadioPtr, char mode);
char Nordic_GetTransceiverMode(XSpi* SPIRadioPtr);
void change_robot_address(XSpi * SPIRadioPtr, char robot_num);
void Nordic_SetRF_Channel(XSpi * SPIRadioPtr, char channel );
inline int Nordic_ProcessPacket(int info[14]);
void Nordic_SetRF_RX_Channel(XSpi * SPIRadioPtr, char channel );
void Nordic_SetRF_RX_Channel(XSpi * SPIRadioPtr, char channel );
char return_error(char n);

#define   HIGH_CHIP 0X0C
#define   MEDIUM_CHIP 0X08
#define   LOW_CHIP 0X04
#define   HIGH_KICK 0X03
#define   MEDIUM_KICK 0X02
#define   LOW_KICK 0X01
#define MAX_KICKBRD_DELAY 1600

#define	HIGH_KICK_DELAY	1400
#define	MEDIUM_KICK_DELAY 200
#define	LOW_KICK_DELAY 150
#define	HIGH_CHIP_DELAY	1000
#define	MEDIUM_CHIP_DELAY 800
#define	LOW_CHIP_DELAY 600

//defines dos filtros corrente e encoder
#define FILTER_ENCODER 0x00
#define FILTER_CURRENT 0x01

#define MAX_SPI_BUF_SIZE 48 //buffer da SPI
typedef struct {
u8 RecvBuffer[MAX_SPI_BUF_SIZE];
u8 TxBuffer[MAX_SPI_BUF_SIZE];
unsigned int RecvBytes;
} SPIDataStruct;

volatile SPIDataStruct SPIData;


#endif /*PERIFERICOS_H_*/
