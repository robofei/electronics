#ifndef TIMERS_H_
#define TIMERS_H_

#include "xtmrctr.h"

void Timer_Configure();
void Delay_ms(int x);
void Delay_us(int x);
void Cronometer_Start();
void Cronometer_Stop();
u32 Cronometer_Read();

#endif /*TIMERS_H_*/
