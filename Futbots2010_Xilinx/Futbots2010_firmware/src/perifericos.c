#include "xparameters.h"
#include "xgpio.h"
#include "xio.h"
#include "xspi.h"
#include "xuartlite.h"
#include "Futbots.h"
#include "perifericos.h"
#include "timers.h"
#include "math.h"
#include "IIRFilter.h"
#include <stdlib.h>

unsigned char RadiopacketID=0;
int badpacket=0;
int goodpacket=0;
int totalpacket=0;
int incpacket=0;
int dblpacket=0;
int sendpacket=0;
int startprocesstime=0;
int endprocesstime=0;
int_char processtime;


int prev_delta[5]={0,0,0,0,0};
int prev_odom[4]={0,0,0,0};
float prev_err[4]={0,0,0,0};

float global_err[5]={0,0,0,0,0};

int motor_dead_zone = MOTOR_DEAD_ZONE_BASE;
//float Kp = 3.0;
float Kp = 7.0;
float Kd = .5;
float Ki = 0.2;

int cont1fir[10], cont2fir[10],cont3fir[10],cont4fir[10];
int cont1_idx=0; int cont2_idx=0; int cont3_idx=0; int cont4_idx=0;

bworth_filter motor_ifilter[5]; //current_filters
float offset_curr[4];

char return_error(char n)
{
	return ((char)global_err[n]);
}

void gpio_bit_write (unsigned long baseaddr, unsigned long bit_index, unsigned long new_val)
{
  unsigned long val;
  unsigned long mask =0;
  mask = 1 << bit_index;

  val = XGpio_ReadReg (baseaddr,0);
  if (new_val)
 	val |= mask;  // set bit
  else
  	val &= ~mask;  // clear bit

  XGpio_WriteReg (baseaddr, 1, val);
}
/*
void gLED_Toggle(){
	if (gled){
		LEDg_off;
	    gled=0;
	}else{
		LEDg_on;
    	gled=1;
    }
}

void yLED_Toggle(){
	if (yled){
		LEDy_off;
	    yled=0;
	}else{
		LEDy_on;
    	yled=1;
    }
}
*/


inline void Nordic_readreg(XSpi * SPIRadioPtr, unsigned char reg){
	SPIData.TxBuffer[0] = R_REGISTER + reg;
	XSpi_Transfer(SPIRadioPtr,(u8*)SPIData.TxBuffer,(u8*)SPIData.RecvBuffer,2);
    while (SPIRadioPtr->IsBusy);
    
}

inline void Nordic_writereg(XSpi * SPIRadioPtr, unsigned char reg, unsigned char value){
	SPIData.TxBuffer[0] = W_REGISTER + reg;
	SPIData.TxBuffer[1] = value;
	XSpi_Transfer(SPIRadioPtr,(u8*)SPIData.TxBuffer,(u8*)SPIData.RecvBuffer,2);
    while (SPIRadioPtr->IsBusy);
}

char Nordic_GetTransceiverMode(XSpi* SPIRadioPtr){
	Nordic_readreg(SPIRadioPtr, _NRF24L01P_REG_CONFIG);
	return SPIData.RecvBuffer[1] & 0x01;
}

void Nordic_SetTransceiverMode(XSpi* SPIRadioPtr, char mode){
	char config_reg;
	Nordic_CE_off; //Needs to be in Standby to configure
	Nordic_readreg(SPIRadioPtr, _NRF24L01P_REG_CONFIG);
	config_reg = SPIData.RecvBuffer[1];
	if (mode == _NRF24L01P_CONFIG_PRIM_RX){
		Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_CONFIG, config_reg | _NRF24L01P_CONFIG_PRIM_RX);
	}else{
		Nordic_readreg(SPIRadioPtr, _NRF24L01P_REG_CONFIG);
		Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_CONFIG, config_reg & ~_NRF24L01P_CONFIG_PRIM_RX);
	}
	Delay_us(130); //stand-by to power-up time
	
}

void Nordic_RadioConfig(XSpi * SPIRadioPtr, char robot_num, char frequency, char oper_mode){
	Nordic_CE_off; //Enters Standby-Mode, if active
	Nordic_readreg(SPIRadioPtr, _NRF24L01P_REG_CONFIG);
    
	//POWER UP + CRC ENABLED + Oper Mode
	Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_CONFIG, 
								_NRF24L01P_CONFIG_PWR_UP + _NRF24L01P_CONFIG_MASK_TX_DS +
								_NRF24L01P_CONFIG_CRC_16BIT);
	//set payload length
	Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_RX_PW_P0, RADIO_PACKET_SIZE); 
	// Disable Auto_ACK
	Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_EN_AA, 0x0);
	// Disable Auto Retry
	Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_SETUP_RETR, 0x0);
	//Set Tx Address for the standard radio on the PC
	Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_TX_ADDR, 0x0F);
	//Set Rx Address
	Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_RX_ADDR_P0, robot_num);
//  Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_RF_SETUP, _NRF24L01P_RF_SETUP_RF_PWR_0DBM + _NRF24L01P_RF_SETUP_RF_DR_250KBPS);
	//Sets TX/RX
	Nordic_SetTransceiverMode(SPIRadioPtr, _NRF24L01P_CONFIG_PRIM_RX);
    if (oper_mode == _NRF24L01P_CONFIG_PRIM_RX) //if RX mode
    	Nordic_CE_on;            	
	
  if(frequency ==0){
  	Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_RF_CH, _NRF24L01P_CHANNEL_A_RX);
  }else{
	Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_RF_CH, _NRF24L01P_CHANNEL_B_RX);
  }
  
}

void Nordic_Transmit(XSpi * SPIRadioPtr, unsigned char * input){
	int n =0;
	unsigned char checksum=0;
	unsigned char returnRX=0;

	if (Nordic_GetTransceiverMode(SPIRadioPtr) == _NRF24L01P_CONFIG_PRIM_RX){
		returnRX = 1;
		Nordic_SetTransceiverMode(SPIRadioPtr, _NRF24L01P_CONFIG_PRIM_TX);
		Nordic_SetRF_TX_Channel(SPIRadioPtr, robot_rf_ch );
	}
	
	SPIData.TxBuffer[0] = W_TX_PAYLOAD;
	//Preamble
	SPIData.TxBuffer[1] = 0x7E; //delimiter
	SPIData.TxBuffer[2]=TX_PACKET_SIZE/256;
	SPIData.TxBuffer[3]=TX_PACKET_SIZE - 4;
	SPIData.TxBuffer[4]=0x90; //tx packet
	SPIData.TxBuffer[5]++; //Frame Counter
	//SPIData.TxBuffer[6] =; //Destination Address
//	SPIData.TxBuffer[6] = (processtime >> 24) & 0xff;
//	SPIData.TxBuffer[7] = (processtime >> 16) & 0xff;
//	SPIData.TxBuffer[8] = (processtime >> 8) & 0xff;
//	SPIData.TxBuffer[9] = (processtime >> 0) & 0xff;
	
	SPIData.TxBuffer[PREAMBLE_SIZ]= 'b'; // byte inicial, parte do protocolo
	SPIData.TxBuffer[TX_PACKET_SIZE - 1] = 'e' ;// byte final, parte do protocolo
	
	for (n=(PREAMBLE_SIZ+1); n < TX_PACKET_SIZE - 1 ;n++){
		SPIData.TxBuffer[n] = input[n - (PREAMBLE_SIZ+1)];

		}
	for (n=3; n< TX_PACKET_SIZE ;n++){//Checksum calculation
		checksum += SPIData.TxBuffer[n];
	}
	checksum = ~checksum;
	SPIData.TxBuffer[TX_PACKET_SIZE] = checksum;
	
	XSpi_Transfer(SPIRadioPtr,(u8*)SPIData.TxBuffer,(u8*)SPIData.RecvBuffer, RADIO_PACKET_SIZE +1);
	while (SPIRadioPtr->IsBusy);
	Nordic_CE_on;
	Delay_us(10);
	Nordic_CE_off;
  	//Time on air delay
	Delay_ms(1); // Time on air for 250Kbps and 32Bytes transmit
	//Delay_us(500);
	
	if (returnRX){ //If original mode was RX, return to it (after the 130us setting period)
		Nordic_SetTransceiverMode(SPIRadioPtr, _NRF24L01P_CONFIG_PRIM_RX);
		Nordic_CE_on; //needed after setting radio to RX
		Nordic_SetRF_RX_Channel(SPIRadioPtr, robot_rf_ch );
	}
	
	
}


int Nordic_GetData(XSpi * SPIRadioPtr, int info[14], unsigned char *packetID){


	startprocesstime = (unsigned int)Cronometer_Read();
	//if (Nordic_GetTransceiverMode(SPIRadioPtr) == _NRF24L01P_CONFIG_PRIM_TX){
	//	Nordic_SetTransceiverMode(SPIRadioPtr, _NRF24L01P_CONFIG_PRIM_RX);
	//	Nordic_CE_on;
	//}
	
	Nordic_readreg(SPIRadioPtr, _NRF24L01P_REG_STATUS);
		
	if (SPIData.RecvBuffer[0] & _NRF24L01P_STATUS_RX_DR){
		if ((SPIData.RecvBuffer[0] & _NRF24L01P_STATUS_RX_P_NO) == (0x0 <<1)){
			SPIData.TxBuffer[0] = R_RX_PAYLOAD; //receives buffer
	    	XSpi_Transfer(SPIRadioPtr, (u8*)SPIData.TxBuffer,(u8*)SPIData.RecvBuffer,RADIO_PACKET_SIZE+1);
   			while (SPIRadioPtr->IsBusy);
   			//Radio_HasData = 1;
		}
		Nordic_readreg(SPIRadioPtr, _NRF24L01P_REG_STATUS); //Reads Status
		if ((SPIData.RecvBuffer[0] & _NRF24L01P_STATUS_RX_P_NO) == (0x7 <<1)){
			//clears INT_RX flag
			Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_STATUS, (1<<6));
		}
		SPIData.RecvBytes = RADIO_PACKET_SIZE+1;
		Nordic_ProcessPacket(info);
//		processtime.i =(unsigned int)Cronometer_Read();
		endprocesstime = (unsigned int)Cronometer_Read();
//		AddtoReturnSensor(processtime.c, 16, sizeof(int));
		//processtime = 0;
		return RADIO_PACKET_SIZE;
	}else{
		//Cronometer_Stop();
		//processtime = 0;
		return 0;
	} 
}

inline int Nordic_ProcessPacket(int info[14]){	
	int n =0;
	totalpacket++;
	int bytesrcvd =	SPIData.RecvBytes;	
	
    if(SPIData.RecvBuffer[4] != 0x90) // checa se o tipo de msg e diferente de RX (eduardo)
    	return 0;
    	
	int byte_start = PREAMBLE_SIZ;
    	
    //checa a integridade do protocolo
    if(SPIData.RecvBuffer[byte_start] == 'b' && SPIData.RecvBuffer[byte_start + 15] == 'e') { 	
    	goodpacket++;
   		//addr=UartData.RecvBuffer[4];
    	
    	//checa se o pacote � destinado ao num do robo
    	if (SPIData.RecvBuffer[byte_start + 1] == robot_num){
			if (gled){
				LEDg_off;
			    gled=0;
			}else{
				LEDg_on;
		    	gled=1;
		    }

			info[0] = (int)((signed char)SPIData.RecvBuffer[byte_start + 2]);//vx (r)
			info[1] = (int)((signed char)SPIData.RecvBuffer[byte_start + 3]);//vy (livre)
			info[2] = (int)((signed char)SPIData.RecvBuffer[byte_start + 4]);//w  (theta)
			info[3] = (int)((unsigned char)SPIData.RecvBuffer[byte_start + 5]);//vref 
    		
    		info[4] = SPIData.RecvBuffer[byte_start + 6]; // command byte1 - kick e roller e flags
    		info[5] = SPIData.RecvBuffer[byte_start + 7]; // command byte2
    		info[6] = SPIData.RecvBuffer[byte_start + 8]; // command byte3 - syncradio
    		info[7] = SPIData.RecvBuffer[byte_start + 9]; //CUSTOM_KICK_DELAY
    		info[8] = SPIData.RecvBuffer[byte_start + 10]; // transmitted (pc -> robot) packet counter
    		info[9] = SPIData.RecvBuffer[byte_start + 11]; //rho / KP_A
    		info[10] = SPIData.RecvBuffer[byte_start + 12]; //rho / KP
    		info[11] = SPIData.RecvBuffer[byte_start + 13]; //rho / KD
    		info[12] = SPIData.RecvBuffer[byte_start + 14]; //rho 
    		RadiopacketID++;
	  }
    }else{
    	badpacket++;
    }
    
    //  *packetID = RadiopacketID;
      
      //reseta os bytes do protocolo
        for (n=0; n < SPIData.RecvBytes; n++){
      	SPIData.RecvBuffer[n] = 0;
      }
      
      SPIData.RecvBytes = 0; // resets the number of bytes received. to find a better place for it !
      return bytesrcvd;
}


void Nordic_SetRXAddr(XSpi * SPIRadioPtr, char robot_num){
	Nordic_CE_off;
	Delay_us(130);
	Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_RX_ADDR_P0, robot_num); //define RX address
	Nordic_CE_on;
	Delay_us(130);
}


void Nordic_SetRF_Channel(XSpi * SPIRadioPtr, char channel ){
  if (channel ==0)
  	channel = _NRF24L01P_CHANNEL_A_RX;
  else
  	channel = _NRF24L01P_CHANNEL_B_RX;
  
  Nordic_CE_off;  
  Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_RF_CH, channel);
  Nordic_CE_on;
}

void Nordic_SetRF_RX_Channel(XSpi * SPIRadioPtr, char channel ){
  if (channel ==0)
  	channel = _NRF24L01P_CHANNEL_A_RX;
  else
  	channel = _NRF24L01P_CHANNEL_B_RX;

  Nordic_CE_off;
  Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_RF_CH, channel);
  Nordic_CE_on;

}

void Nordic_SetRF_TX_Channel(XSpi * SPIRadioPtr, char channel ){
  if (channel ==0)
  	channel = _NRF24L01P_CHANNEL_A_TX;
  else
  	channel = _NRF24L01P_CHANNEL_B_TX;

  Nordic_CE_off;
  Nordic_writereg(SPIRadioPtr, _NRF24L01P_REG_RF_CH, channel);
  Nordic_CE_on;

}

char DIP_Switch(){
	char testedechar = (char) (XGpio_ReadReg(XPAR_DIPANDBUTTONS_7BIT_BASEADDR, 1));
	return (char) (XGpio_ReadReg(XPAR_DIPANDBUTTONS_7BIT_BASEADDR, 1) & 0x07);
}

char DIP_Frequency(){
	return (char) (XGpio_ReadReg(XPAR_DIPANDBUTTONS_7BIT_BASEADDR, 1) & 0x08);
}

char BallSensor_Status(){
	return (char) (XGpio_ReadReg(XPAR_SENSORSANDACTUATORS_4BIT_BASEADDR, 1) & 0x08);
}

char ShootPower_Status(){
	//rechecar se pino continua o mesmo e se funcao deve existir - 15/fev/2011
	return (char) (XGpio_ReadReg(XPAR_SENSORSANDACTUATORS_4BIT_BASEADDR, 1) & 0x04);
}

void SetPIDPars(float _kp, float _kd){
	Kp = _kp;
	Kd = _kd;
}

void SetRoller(int val){
	int setpoint = val;
	if (setpoint > 90)
		setpoint = 90;
	else if (setpoint < -90)
		setpoint = -90;
		
	if (setpoint > - 127 && setpoint < 127){ 		 //checa limite de 8bits
		if (setpoint < 0){	 //caso a velocidade seja negativa
    		setpoint = (abs(setpoint)) | 0x80;
    	}
	    XGpio_WriteReg (XPAR_BLDCMOTOR_5_BASEADDR, 1, setpoint);
	}
}

void KickBrd_Activate(int mode, int active_timer){
	// mode controla o tipo de chute (kick ou chip). deve ser 0 para kick e 1 para chip
	// timer * 10us � o tempo que o dispositivo ficara ativado. max = 4095
	if (active_timer > 0 && active_timer < 4096 && (mode == 0 || mode == 1)){
		XGpio_WriteReg (XPAR_KICKBOARD_0_BASEADDR, 1, mode * 0x1000 + active_timer);
	}
}

inline int getOdometry(int n_wheel){
	int count=0;
	switch(n_wheel){
	    case 1:
			count = XGpio_ReadReg (XPAR_ENCODER_1_BASEADDR, 1);
//			cont1fir[cont1_idx] = count/10;
//			cont1_idx = (cont1_idx + 1)%10;
//			count=0;
//			for (n=0;n<10;n++){
//				count+=cont1fir[n];
//			}
			//count = (int)Butterworth_LP2_Process((float)count, &encoder1_filter, FILTER_CURRENT);
	    	break;
   	    case 2:
			count = XGpio_ReadReg (XPAR_ENCODER_2_BASEADDR, 1);
//			cont2fir[cont2_idx] = count/10;
//			cont2_idx = (cont2_idx + 1)%10;
//			count=0;
//			for (n=0;n<10;n++){
//				count+=cont2fir[n];
//			}
			//count = (int)Butterworth_LP2_Process((float)count, &encoder2_filter, FILTER_CURRENT);
	    	break;
	    case 3:
	    	count = XGpio_ReadReg (XPAR_ENCODER_3_BASEADDR, 1);
//	    	cont3fir[cont3_idx] = count/10;
//			cont3_idx = (cont3_idx + 1)%10;
//			count=0;
//			for (n=0;n<10;n++){
//				count+=cont3fir[n];
//			}
			//count = (int)Butterworth_LP2_Process((float)count, &encoder3_filter, FILTER_CURRENT);
	    	break;
	    case 4:
			count = XGpio_ReadReg (XPAR_ENCODER_4_BASEADDR, 1);
//			cont4fir[cont4_idx] = count/10;
//			cont4_idx = (cont4_idx + 1)%10;
//			count=0;
//			for (n=0;n<10;n++){
//				count+=cont4fir[n];
//			}
			//count = (int)Butterworth_LP2_Process((float)count, &encoder4_filter, FILTER_CURRENT);
	    	break;
	    }

	// se a contagem tem um valor muito alto, � porque esta no sentido inverso
	// remover o fundo de escala (0xFFFF ou 65535) obtem o valor no sentido oposto (negativo)
	if (count > 0x8000)
		count -= 0xFFFF;
	
	//rudimentar anti-noise
	if (abs(count) > 200)
		count = prev_odom[n_wheel-1];
	
	prev_odom[n_wheel-1] = count; 
		
	return count;
}


void SetDeadZone(int val){
	motor_dead_zone = val;
}

float Motor_AccelAdjust(float val_f, float prev_val, float limit){
	if (val_f > prev_val)
		return prev_val + limit;
	else
		return prev_val - limit;	
}

void SetMotor(char nmotor, int val){
	int setpoint;
	setpoint = val;
	
	//ajuste para redistribuir valores de porcentagem de velocidade entre o range acima da zona morta
	setpoint = (setpoint * (100 - motor_dead_zone))/100; 
	if (setpoint > 0)
		setpoint += motor_dead_zone;
	else if (setpoint <0)
		setpoint -= motor_dead_zone;
	
    //limitador de potencia do motor
	if (setpoint >= 100)
		setpoint = 100;
	else if (setpoint <= -100)
		setpoint = - 100;
		
	if (setpoint > 0){	 //caso a velocidade seja negativa //? rotacao motores invertida no vhdl ???????
   		setpoint = (abs(setpoint)) | 0x80;
   	}else if (setpoint < 0){
		setpoint = abs(setpoint);
	}
	
    switch(nmotor){
    case 1:
    	XGpio_WriteReg (XPAR_BLDCMOTOR_1_BASEADDR, 1, setpoint);
    	break;
    case 2:
	   	XGpio_WriteReg (XPAR_BLDCMOTOR_2_BASEADDR, 1, setpoint);
	   	break;
	case 3:
	   	XGpio_WriteReg (XPAR_BLDCMOTOR_3_BASEADDR, 1, setpoint);
	  	break;
	case 4:
	   	XGpio_WriteReg (XPAR_BLDCMOTOR_4_BASEADDR, 1, setpoint);
	  	break;
	}
}

int PIDRoller(int setpoint){
	float Kp_roller = 1.5;
	int scale = 4;
	int ctrlvalue;
	int odom;
	int delta_odom;
	odom = (int)XGpio_ReadReg (XPAR_BLDCMOTOR_5_BASEADDR, 1);
    delta_odom = -odom + prev_delta[4] ;
    prev_delta[4] = odom;
    delta_odom = delta_odom;
    if (abs(delta_odom) > 250)
	    delta_odom = 0;
	    
    ctrlvalue = scale*(int)(Kp_roller * (float)(setpoint/scale - delta_odom));
    
    //evita que haja esforco na direcao contraria, motor do roller nao necessita
    if ( (setpoint >0 && ctrlvalue <0) || (setpoint <0 && ctrlvalue > 0) )
	    ctrlvalue =0;
	    
    SetRoller(ctrlvalue);

    return delta_odom;
}

int PIDMotor(char nmotor, int setpoint){

	int ctrlvalue, odom;
	float err;
	//float INTEG_ADD_LIMIT = .5;
	float val_f = (float)setpoint;
	float setpoint_f;
	static float prev_val[4]={0,0,0,0}; //
	static float accum_err[4]={0,0,0,0}; //acumulador/integrador

	val_f = (float)setpoint;

/****Corrente max do motor -> 2,58A limitar em 2 a 2,2A ****/
	
	float DV_LIMIT_A = 0.6;
	if (fabs(val_f - prev_val[nmotor-1]) > DV_LIMIT_A){
		if (val_f > prev_val[nmotor-1])
			setpoint_f = prev_val[nmotor-1] + DV_LIMIT_A;
		else
			setpoint_f = prev_val[nmotor-1] - DV_LIMIT_A;
	}else{
		setpoint_f = val_f;
		}
	prev_val[nmotor-1] = setpoint_f;
	setpoint = (int)setpoint_f;
	//
	
	odom = getOdometry(nmotor);
	
	err = (float)(setpoint - odom);
	global_err[nmotor] = err;

	if (fabs(val_f - prev_val[nmotor-1] > 5) || setpoint ==0) //diferenca mto grande, nao integrar
		accum_err[nmotor-1]=0;
	
	ctrlvalue = (int)((Kp * err));// + (Ki * accum_err[nmotor-1])) ;
	
	/*if (err > INTEG_ADD_LIMIT)
		accum_err[nmotor-1] += INTEG_ADD_LIMIT;
	else if (err < -INTEG_ADD_LIMIT)
			accum_err[nmotor-1] -= INTEG_ADD_LIMIT;
	else
		accum_err[nmotor-1] += err;
	if (accum_err[nmotor-1] > 25.)
		accum_err[nmotor-1] =25.;
	else if (accum_err[nmotor-1] < -25.)
		accum_err[nmotor-1] =-25.;
	//*/
	
	prev_err[nmotor-1] = err;  //erro anterior, usado no calculo do termo derivativo

	//prevenir inversoes significativas na rotacao do motor. 
	//valor limitado serve como freio motor leve
	#define MOTOR_INV_LIMIT 10
	if (odom > MOTOR_INV_LIMIT && ctrlvalue < -MOTOR_INV_LIMIT)
		ctrlvalue = -MOTOR_INV_LIMIT;
	else if (odom < -MOTOR_INV_LIMIT && ctrlvalue >MOTOR_INV_LIMIT)
		ctrlvalue = MOTOR_INV_LIMIT;
	//
	
    SetMotor(nmotor, ctrlvalue);

    return odom;
}

int Read_AD7928(int channel){
	int read=0;
	if (channel <0 || channel > 5) //checa se o canal � valido, retorna 0 se nao for.
		return 0;
	
	//leitura:
	//channel * 0x4 converte o valor de canal para a representacao por bit requerida pelo modulo VHDL (0,0x4,0x8,0xC....)	
	read = (int)XGpio_ReadReg (XPAR_AD7928_0_BASEADDR, channel * 0x4);

	if ((read >> 12) !=channel) //verif se os bits que indicam o canal lido conferem
		return 0; //colocar um breakpoint aqui. se entrar tem algo errado
		
	read = read & 0x0FFF; //mantem apenas os 12 primeiros bits da leitura - AD7928 tem 12bits
	return read;
}
/*** offset da leitura para os sensores de corrente, deve-se alimentar o filtro antes de ter o valor correto de offset ***/
void Set_curr_offset(){
	int i;
	for(i=0; i<100; i++)
	{
		offset_curr[0] = 2048 - Butterworth_LP2_Process((float)Read_AD7928(1), &motor_ifilter[0],FILTER_CURRENT);
		offset_curr[1] = 2048 - Butterworth_LP2_Process((float)Read_AD7928(2), &motor_ifilter[1],FILTER_CURRENT);
		offset_curr[2] = 2048 - Butterworth_LP2_Process((float)Read_AD7928(3), &motor_ifilter[2],FILTER_CURRENT);
		offset_curr[3] = 2048 - Butterworth_LP2_Process((float)Read_AD7928(4), &motor_ifilter[3],FILTER_CURRENT);
		Delay_us(10);
	}
}

/*** Funcao que le a corrente do motor pelo AD. A formula da corrente eh i = -37.5 + 0.01831*Read_AD7928 (A).
 O meio da escala do ad eh 2048 para 2.5v de leitura do sensor ***/
float Read_Current (int n){
	return (Butterworth_LP2_Process((float)Read_AD7928(n+1), &motor_ifilter[n],FILTER_CURRENT) + offset_curr[n]) ;
}

